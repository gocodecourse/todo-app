let works = [];
const worksTable = document.querySelector(".works-table");
const worksDisplay = document.querySelector(".works-display");
const toggleBtn = document.querySelector(".toggle");
let mode = toggleBtn.innerText;

function getCheckSignElement(work, workDesc, workButtons, multiSign) {
  const checkSign = document.createElement("button");
  checkSign.className =
    mode === "Dark Mode"
      ? "check-sign light-mode-not-done"
      : "check-sign dark-mode-not-done";
  checkSign.innerHTML = "&#10004";
  checkSign.addEventListener("click", function (e) {
    setIsDoneToTrue(workDesc);
    setNewClassName(work, workDesc, workButtons, multiSign, checkSign);
  });

  return checkSign;
}

function setIsDoneToTrue(workDesc) {
  works.forEach(function (work) {
    if (work.name === workDesc.innerText) {
      work.isDone = true;
    }
  });
}

function setNewClassName(work, workDesc, workButtons, multiSign, checkSign) {
  work.className = mode === "Dark Mode" ? "light-mode-done" : "dark-mode-done";
  workDesc.className =
    mode === "Dark Mode" ? "work-desc light-mode-done" : "work-desc dark-mode-done";
  workButtons.className =
    mode === "Dark Mode" ? "btns light-mode-done" : "btns dark-mode-done";
  checkSign.className =
    mode === "Dark Mode" ? "check-sign light-mode-done" : "check-sign dark-mode-done";
  multiSign.className =
    mode === "Dark Mode" ? "multi-sign light-mode-done" : "multi-sign dark-mode-done";
}

function getMultiSignElement(work, workDesc) {
  const multiSign = document.createElement("button");
  multiSign.className =
    mode === "Dark Mode"
      ? "multi-sign light-mode-not-done"
      : "multi-sign dark-mode-not-done";
  multiSign.innerHTML = "&#10008";
  multiSign.addEventListener("click", function (e) {
    works = works.filter(function (work) {
      return work.name !== workDesc.innerText;
    });
    work.remove();
  });

  return multiSign;
}

function connectElements(worksTable, work, workButtons, multiSign, checkSign, workDesc) {
  work.append(workDesc);
  workButtons.append(checkSign);
  workButtons.append(multiSign);
  work.append(workButtons);
  worksTable.append(work);
}

function addWorkInputToWorksTable(workInput) {
  const work = document.createElement("li");
  work.className = mode === "Dark Mode" ? "light-mode-not-done" : "dark-mode-not-done";

  const workDesc = document.createElement("span");
  workDesc.className =
    mode === "Dark Mode"
      ? "work-desc light-mode-not-done"
      : "work-desc dark-mode-not-done";
  workDesc.innerText = workInput;

  const workButtons = document.createElement("span");
  workButtons.className =
    mode === "Dark Mode" ? "btns light-mode-not-done" : "btns dark-mode-not-done";

  const multiSign = getMultiSignElement(work, workDesc);
  const checkSign = getCheckSignElement(work, workDesc, workButtons, multiSign);

  connectElements(worksTable, work, workButtons, multiSign, checkSign, workDesc);

  works.push({
    name: workInput,
    element: work,
    isDone: false,
  });
}

function getWorkInput() {
  return document.querySelector(".new-work-input").value.trim();
}

function resetWorkInput() {
  document.querySelector(".new-work-input").value = "";
}

function isWorkInputValid(workInput) {
  isExist = false;
  works.forEach(function (work) {
    if (work.name === workInput) {
      isExist = true;
    }
  });
  return workInput === "" || isExist;
}

function addWork() {
  mode = toggleBtn.innerText;
  const workInput = getWorkInput();

  if (isWorkInputValid(workInput)) {
    return;
  }

  addWorkInputToWorksTable(workInput);
  resetWorkInput();
}

const addWorkBtn = document.querySelector(".add-work-btn");
addWorkBtn.addEventListener("click", function () {
  addWork();
});

const input = document.querySelector(".new-work-input");
input.addEventListener("keypress", function (e) {
  if (e.key === "Enter") {
    addWork();
  }
});

worksDisplay.addEventListener("change", function (e) {
  if (e.target.value === "All") {
    works.forEach(function (work) {
      worksTable.append(work.element);
    });
  } else if (e.target.value === "Done") {
    removeEntireTable();
    addNewTable(true);
  } else if (e.target.value === "Not Done") {
    removeEntireTable();
    addNewTable(false);
  }
});

function removeEntireTable() {
  [...worksTable.children].forEach(function (work) {
    worksTable.removeChild(work);
  });
}

function addNewTable(isDone) {
  works.forEach(function (work) {
    if (work.isDone === isDone) {
      worksTable.append(work.element);
    }
  });
}

toggleBtn.addEventListener("click", function (e) {
  if (mode === "Dark Mode") {
    changeMode(`.light-mode-done, .light-mode-not-done`, "light", "dark");
    changeToggleBtnStyle();
  } else {
    changeMode(`.dark-mode-done, .dark-mode-not-done`, "dark", "light");
    changeToggleBtnStyle();
  }
});

function changeMode(classNames, from, to) {
  // Amit - start
  // why isn't it working?
  // const workElements = worksTable.querySelectorAll("[className*=mode]");
  // Amit - end

  const workElements = worksTable.querySelectorAll(classNames);
  [...workElements].forEach(function (element) {
    element.className = element.className.replace(from, to);
  });
}

function changeToggleBtnStyle() {
  if (mode === "Dark Mode") {
    mode = "Light Mode";
    toggleBtn.innerText = "Light Mode";
    toggleBtn.style.background = "rgb(227, 227, 227)";
    toggleBtn.style.color = "black";
    toggleBtn.style.boxShadow = "0px 0px 5px 0px rgba(0, 0, 0, 0.5)";
  } else {
    mode = "Dark Mode";
    toggleBtn.innerText = "Dark Mode";
    toggleBtn.style.background = "rgb(67, 67, 67)";
    toggleBtn.style.color = "rgb(220, 220, 220)";
    toggleBtn.style.boxShadow = "0px 0px 5px 0px rgba(255, 255, 255, 0.5)";
  }
}
